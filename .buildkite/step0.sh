#!/bin/bash

# This fetches the data that was filled out in the previous block
# step and outputs the values

set -eu
ls -l
vers=$(sudo docker run --rm -v $BUILDKITE_BUILD_CHECKOUT_PATH:/repo ppvs/build:gitver /nocache |  jq -r '.SemVer') || true
if [[ -z $vers ]]; then
version=$BUILDKITE_BUILD_NUMBER
else
version=${vers}${v}
fi
buildkite-agent meta-data set release-name $version
echo $version
echo $Deploy
echo $version
echo $v