#!/bin/bash

# This fetches the data that was filled out in the previous block
# step and outputs the values

set -eu

RELEASE_NAME=$(buildkite-agent meta-data get release-name)
sleep 10
echo $RELEASE_NAME
if [[ $BUILDKITE_PULL_REQUEST != "false" ]]; then
echo building artifacts
fi
echo $Deploy