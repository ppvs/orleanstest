#!/bin/bash

# Note that we don't enable the 'e' option, which would cause the script to
# immediately exit if 'run_tests' failed
set -uo pipefail

echo 1
# Run the main command we're most interested in
cd reerre

echo 2
# Capture the exit status
TESTS_EXIT_STATUS=$?

# Run additional commands
./clean_up.sh

# Exit with the status of the original command
exit $TESTS_EXIT_STATUS
